# Issues Дизайн Системы ВШЭ

- [Сообщить о баге](https://gitlab.com/proscom/hse-design-issues/-/issues/new?issuable_template=Bug)

- [Запрос на добавление нового функционала](https://gitlab.com/proscom/hse-design-issues/-/issues/new?issuable_template=Feature)

Пожалуйста, придерживайтесь шаблона Issue
